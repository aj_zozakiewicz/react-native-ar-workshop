import { AppRegistry } from 'react-native'
import App from './src/App'

AppRegistry.registerComponent('ReactMeetupARApp', () => App)

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('ViroSample', () => App)
