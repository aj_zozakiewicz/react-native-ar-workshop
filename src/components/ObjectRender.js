import React from 'react'
import { connect } from './../context/AppContext'
import _map from 'lodash/map'
import {
  ViroARScene,
  ViroARTrackingTargets,
  ViroARImageMarker,
  Viro3DObject,
  ViroAmbientLight,
  ViroDirectionalLight
} from 'react-viro'

const threeDTargetConfig = {
  targetTree: {
    source: require('./../assets/ar_marker_2.jpg'),
    orientation: 'Up',
    physicalWidth: 0.1 // real world width in meters
  }
}

ViroARTrackingTargets.createTargets(threeDTargetConfig)

export const ObjectRender = ({ context }) => {
  return (
    <ViroARScene>
      <ViroAmbientLight color="#FFFFFF" />
      <ViroDirectionalLight color="#FFFFFF"
          direction={[0, -1, 0]}
          shadowNearZ={2}
          shadowFarZ={9}
          castsShadow={true} 
      />
      <ViroARImageMarker target='targetTree'>
        <Viro3DObject
          source={require('./../assets/tree/ChristmasTree.obj')}
          resources={[
            require('./../assets/tree/ChristmasTree.mtl')
          ]}
          highAccuracyEvents={true}
          position={[0, 0, 0]}
          scale={[.04, .04, .04]}
          rotation={[0, 0, 0]}
          type="OBJ"
        />
      </ViroARImageMarker>
    </ViroARScene>
  )
}

module.exports = connect(ObjectRender)
