import React from 'react'
import { connect } from '../context/AppContext'
import _map from 'lodash/map'
import {
  ViroARScene,
  ViroARTrackingTargets,
  ViroARImageMarker,
  ViroImage
} from 'react-viro'

const targetConfig = {
  targetOne: {
    source: require('./../assets/ar_marker.jpg'),
    orientation: 'Up',
    physicalWidth: 0.1, // real world width in meters
    copy: 'Hey, you found target one! This is some cool info about target 1.'
  },
  targetTwo: {
    source: require('./../assets/ar_marker_1.jpg'),
    orientation: 'Up',
    physicalWidth: 0.1, // real world width in meters
    copy: 'Hey, you found target two! This is some cool info about this target.'
  }
}

ViroARTrackingTargets.createTargets(targetConfig)

export const TargetTrackingSceneAR = ({ context }) => {
  return (
    <ViroARScene>
      { _map(targetConfig, (target, index) => (
        <ViroARImageMarker
          key={index}
          target={index}
          onAnchorUpdated={() => context.updateMarker(index)}
          
        >
          {
            context.selectedMarker === index && <ViroImage
              height={0.05}
              width={0.05}
              rotation={[-90, 0, 0]}
              position={[-.04, .1, 0]}
              source={require('./../assets/info-bubble.png')}
              onClick={() => context.toggleDialog(target.copy)}
            />
          }
        </ViroARImageMarker>
      ))}
    </ViroARScene>
  )
}

module.exports = connect(TargetTrackingSceneAR)
