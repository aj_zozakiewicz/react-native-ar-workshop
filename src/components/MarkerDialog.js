import React from 'react'
import { connect } from '../context/AppContext'
import { Card, Text, Button, CardHeader } from '@ui-kitten/components'
import styled from 'styled-components/native'

const MarkerDialogCard = styled(Card)`
  width: 90%;
  position: absolute;
  bottom: 5%;
  left: 5%;
`

const StyledText = styled(Text)`
  margin-top: 10;
  margin-bottom: 20;
`

const MarkerDialog = ({ context }) => context.dialogVisible && (
  <MarkerDialogCard header={() => <CardHeader title={context.selectedMarker} />}>
    <StyledText>{context.dialogCopy}</StyledText>
    <Button onPress={context.toggleDialog}>Close</Button>
  </MarkerDialogCard>
)

export default connect(MarkerDialog)