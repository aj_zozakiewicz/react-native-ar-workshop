import React from 'react'
import { AppContextProvider } from './context/AppContext'
import { ApplicationProvider } from '@ui-kitten/components'
import { mapping, light as lightTheme } from '@eva-design/eva'
import styled from 'styled-components/native'
import {
  ViroARSceneNavigator
} from 'react-viro'

import MarkerDialog from './components/MarkerDialog'
import TargetTrackingView from './components/TargetTrackingView'
// import ObjectRender from './components/ObjectRender'

const Wrapper = styled.View`
  flex: 1;
`

const App = () => (
  <ApplicationProvider mapping={mapping} theme={lightTheme}>
    <AppContextProvider>
      <Wrapper>
        <ViroARSceneNavigator initialScene={{ scene: TargetTrackingView }} />
      </Wrapper>
      <MarkerDialog />
    </AppContextProvider>
  </ApplicationProvider>
)

export default App
