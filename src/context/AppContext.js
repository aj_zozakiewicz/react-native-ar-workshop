import React from 'react'

const AppContext = React.createContext({})
  
export class AppContextProvider extends React.Component {
  constructor () {
    super()
    this.state = {
      dialogVisible: false,
      dialogCopy: '',
      selectedMarker: null,
      toggleDialog: this.toggleDialog.bind(this),
      setContext: this.setContext.bind(this),
      updateMarker: this.updateMarker.bind(this)
    }
  }     
    
  toggleDialog (dialogCopy) {
    if (!this.state.dialogVisible) {
      this.setState({ dialogVisible: true,  dialogCopy })
    } else {
      this.setState({ dialogVisible: false, selectedMarker: null, dialogCopy: '' })
    }
  }

  setContext (newContext) {
    this.setState({
     ...this.state,
     ...newContext
    })
  }

  // Hackery to get image bubbles off screen when they are not in view.
  updateMarker (markerId) {
    if (markerId !== this.state.selectedMarker) {
      this.setState({
        selectedMarker: markerId
      })
    }

    // If a new marker is not found within n seconds, clear image bubble
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({ 
        selectedMarker: null,
        dialogVisible: false
      })
    }, 5000)
  }

  render () {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export const connect = (WrappedComponent) => () => (
  <AppContext.Consumer>
    {context => <WrappedComponent context={context} />}
  </AppContext.Consumer>
)
