# react-native-ar-workshop

## Getting Started

1. Install Debug App - [‎Viro Media on the App Store](https://apps.apple.com/us/app/viro-media/id1163100576)
2. Ensure your phone and laptop are connected to the same wifi network.
3. Clone Repo
4. `npm i` (node v12.13.1 / npm 6.12.1)
5. `npm run start`
6. Open Viro Media app on your phone and open test bed in the menu.
7. Either enter then ngrok URL from the console or the IP address of your computer using `ifconfig en0`
	* note: debugging js remotely on iOS only seems to work with the IP address method. This is a known issue with Viro React

## Compatible Devices

ViroReact uses ARKit for iOS and ARCore for Android. Please make sure you are running this solution on a compatible device.

### iOS
According to Apple, in order to use ARKit, iOS devices need to have an A9, A10 or A11 chip beating at their heart. While certainly not all iPhones or iPads in out there will meet that criteria, there should be plenty of people for which ARKit development will prove worthwhile.

The devices that use A9, A10 and A11 chips are:

iPhone 6s and 6s Plus
iPhone 7 and 7 Plus
iPhone SE
iPad Pro (9.7, 10.5 or 12.9) – both first-gen and 2nd-gen
iPad (2017)
iPhone 8 and 8 Plus
iPhone X

### Android
[ARCore Compatible Devices](https://developers.google.com/ar/discover/supported-devices)